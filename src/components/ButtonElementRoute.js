import styled from 'styled-components';
import { Link as LinkR } from 'react-router-dom';

export const ButtonRoute = styled(LinkR)`
    border-radius: 50px;
    background: ${({ primary }) => (primary ? '#FF00B6' : '#00FFC6')};
    white-space: nowrap;
    padding: ${({ big }) => (big ? '14px 48px' : '12px 30px')};
    color: ${({ dark }) => (dark ? '#00FFC6' : '#FF00B6')};
    font-size: ${({ fontBig }) => (fontBig ? '20px' : '18px')};
    outline: none;
    border: none;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    transition: all 0.2s ease-in-out;
    font-weight: 500;
    text-decoration: none;

    &:hover {
        transition: all 0.2s ease-in-out;
        background: ${({ primary }) => (primary ? 'white' : 'white')};
        color: #FF00B6;
        transform: scale(1.10);
    }
`;
