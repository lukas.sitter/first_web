import React from 'react';
import VideoBackground from '../../videos/backgroundVideo.mp4';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Container = styled.div`
    min-height: 692px;
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
    top: 0;
    z-index: 0;
    overflow: hidden;
    background: black;
`;

export const FormWrap = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;

    @media screen and (max-width: 480px) {
        height: 80%;
    }    
`;

export const Icon = styled(Link)`
    margin-left: 0px;
    margin-top: 70px;
    margin-bottom: 0px;
    text-decoration: none;
    color: #FF00B6;
    font-weight: 700;
    font-size: 40px;
    text-align: center;
    z-index: 1;

    @media screen and (max-width: 480px) {
        margin-left: 0px;
        margin-top: 8px;
    }
  
    &:hover {
        transition: all 0.2s ease-in-out;
        color: #00FFC6;
        transform: scale(1.10);
    }
`;

export const FormContent = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;

    @media screen and (max-width: 480px) {
        padding: 10px;
    }
 `;

export const Form = styled.form`
    background: transparent;
    max-width: 480px;
    height: auto;
    width: 100%;
    z-index: 1;
    display: grid;
    margin: 0 auto;
    padding: 30px 32px;
    border-radius: 0px;
    margin-bottom: 108px;

    @media screen and (max-width: 480px) {
        padding: 32px 32px;
    }
`;

export const FormH1 = styled.h1`
    margin-bottom: 50px;
    color: #00FFC6;
    font-size: 32px;
    font-weight: 600;
    text-align: center;
`;

export const FormLabel = styled.label`
    margin-bottom: 8px;
    font-size: 16px;
    color: #00FFC6;
    text-align: center;
`;

export const FormInput = styled.input`
    padding: 16px 16px;
    margin-bottom: 32px;
    border: none;
    border-radius: 4px;
    background: white;
    color: #FF00B6;

     &:hover {
        transition: all 0.2s ease-in-out;
        background: #FF00B6;
        color: #00FFC6;  
     }
`;

export const FormButton = styled.button`
    background: #00FFC6;
    padding: 16px 0;
    border: none;
    border-radius: 4px;
    color: #FF00B6;
    font-size: 20px;
    cursor: pointer;

    &:hover {
        transition: all 0.2s ease-in-out;
        background: #FF00B6;
        color: #00FFC6;
        transform: scale(1.02); 
    } 
`;

export const Text = styled(Link)`
    text-align: center;
    margin-top: 24px;
    color: white;
    font-size: 14px;
    text-decoration: none;
    z-index: 2;

    &:hover {
        transition: all 0.2s ease-in-out;
        color: #FF00B6;
        transform: scale(1.10);  
     }
`;

export const VideoBg = styled.video`
    width: 100%;
    height: 100%;
    -o-object-fit: cover;
    object-fit: cover;
    background: #232a34;
    display: block; 
`;

export const PasswordBg = styled.div`
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    overflow: hidden;
`;

const Password = () => (
  <>
    <Container>
      <PasswordBg>
        <VideoBg autoPlay loop muted playsInline src={VideoBackground} type="video/mp4" />
      </PasswordBg>
      <FormWrap>
        <Icon to="/">Cyberpunk</Icon>
        <FormContent>
          <Form action="#">
            <FormH1>Forgot password?</FormH1>
            <FormLabel htmlFor="for">Email</FormLabel>
            <FormInput type="email" required />
            <FormButton type="submit">Continue</FormButton>
            <Text to="/signin">Sign In</Text>
            <Text to="/signup">Create new account</Text>
          </Form>
        </FormContent>
      </FormWrap>
    </Container>
  </>
);

export default Password;
