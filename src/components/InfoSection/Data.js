/* eslint-disable global-require */
export const homeObjOne = {
  id: 'planet',
  lightBg: true,
  lightText: false,
  lightTextDesc: false,
  topLine: 'Year 2063',
  headline: 'Check our planet',
  description: 'Welcome to our planet and feel free to discover all places',
  buttonLabel: 'Learn more',
  imgStart: false,
  img: require('../../images/svg-1.svg'),
  alt: 'Planet',
  primary: true,
  darkText: false,
};

export const homeObjTwo = {
  id: 'city',
  lightBg: false,
  lightText: true,
  lightTextDesc: false,
  topLine: 'Year 2063',
  headline: 'Check our modern city',
  description: 'Welcome to our beautiful city!',
  buttonLabel: 'Learn more',
  imgStart: true,
  img: require('../../images/svg-6.svg'),
  alt: 'City',
  primary: false,
  darkText: true,
};

export const homeObjThree = {
  id: 'signup',
  lightBg: true,
  lightText: false,
  lightTextDesc: false,
  topLine: 'Year 2063',
  headline: 'Sign Up',
  description: 'Sign up to our futuristic platform',
  buttonLabel: 'Learn more',
  imgStart: false,
  img: require('../../images/svg-2.svg'),
  alt: 'SignUp',
  primary: true,
  darkText: false,
};
