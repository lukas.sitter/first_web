import React from 'react';
import ScrollToTop from '../components/ScrollToTop';
import SignIn from '../components/Signin/signin';

const SigninPage = () => (
  <>
    <ScrollToTop />
    <SignIn />
  </>
);

export default SigninPage;
