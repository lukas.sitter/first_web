import React from 'react';
import ScrollToTop from '../components/ScrollToTop';
import SignUp from '../components/Signup/Signup';

const SignupPage = () => (
  <>
    <ScrollToTop />
    <SignUp />
  </>
);

export default SignupPage;
