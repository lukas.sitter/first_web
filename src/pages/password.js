import React from 'react';
import ScrollToTop from '../components/ScrollToTop';
import Password from '../components/Forget password/password';

const PasswordPage = () => (
  <>
    <ScrollToTop />
    <Password />
  </>
);

export default PasswordPage;
