/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react';
import Sidebar from '../components/Sidebar/sidebar';
import Navbar from '../components/Navbar/navbar';
import HeroSection from '../components/HeroSection/herosection';
import InfoSection from '../components/InfoSection/info';
import { homeObjOne, homeObjTwo, homeObjThree } from '../components/InfoSection/Data';
import Services from '../components/Services/services';
import Footer from '../components/Footer/footer';

const Home = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    setIsOpen(!isOpen);
  };

  return (
    <>
      <Sidebar isOpen={isOpen} toggle={toggle} />
      <Navbar toggle={toggle} />
      <HeroSection />
      <InfoSection {...homeObjOne} />
      <InfoSection {...homeObjTwo} />
      <Services />
      <InfoSection {...homeObjThree} />
      <Footer />
    </>
  );
};

export default Home;
