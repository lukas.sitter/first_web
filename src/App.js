import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import Home from './pages/homepage';
import SigninPage from './pages/signin';
import SignupPage from './pages/signup';
import PasswordPage from './pages/password';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/signin" component={SigninPage} exact />
        <Route path="/signup" component={SignupPage} exact />
        <Route path="/resetPassword" component={PasswordPage} exact />
      </Switch>
    </Router>
  );
}

export default App;

// merge
